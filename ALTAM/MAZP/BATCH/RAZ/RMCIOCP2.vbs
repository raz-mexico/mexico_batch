Const ForReading = 1    
Const ForWriting = 2
strFileName = Wscript.Arguments(0)
strFechaOperacion = Wscript.Arguments(1)
strInterfaz = "CAPT2"
strSep=";"
strPais="1"
strFecha = DateAdd("d",-1,strFechaOperacion)
strAnio=DatePart("yyyy",strFecha)
strMes=DatePart("m",strFecha)
strDia=DatePart("d",strFecha)

If strMes<10 Then
strMes="0" & strMes 
End If

If strDia<10 Then
strDia="0" & strDia 
End If

strFechaSinGuion = strAnio &  strMes & strDia

Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objFile = objFSO.OpenTextFile(strFileName, ForReading)

strText = objFile.ReadAll
objFile.Close

strNewText = Replace(strText, "'", "")
strNewText1 = Replace(strNewText, """", "")
strNewText2 = Replace(strNewText1, VbCrLf, strSep & strPais & strSep & strFechaSinGuion & strSep & strInterfaz & VbCrLf)

Set objFile = objFSO.OpenTextFile(strFileName, ForWriting)
objFile.Write strNewText2
objFile.Close
