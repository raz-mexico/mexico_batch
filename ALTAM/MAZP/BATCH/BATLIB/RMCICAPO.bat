set unidadControlM=D
 
%unidadControlM%:
CD\
CD ALTAM\MAZP\BATCH\RAZ\

set txt=RAZ_BD_MEX.config

for /F "tokens=2 delims= " %%i in ('findstr "server=" %txt%') do (
	set server_config=%%i)
for /F "tokens=2 delims= " %%i in ('findstr "puerto=" %txt%') do (
	set puerto_config=%%i)
for /F "tokens=2 delims= " %%i in ('findstr "user=" %txt%') do (
	set user_config=%%i)	
for /F "tokens=2 delims= " %%i in ('findstr "pass=" %txt%') do (
	set pass_config=%%i)
	
set server=%server_config%
set puerto=%puerto_config%
set user=%user_config%
set pass=%pass_config%
set usuario=Control-M
set carpetaM=%unidadControlM%:\ALTAM\MAZP\BATCH\RAZ\

set bd_tablero=TABLERO_MEXICO
set bd_sicre=SICRE_MEXICO
set tabla_final=INT_CAPTACION_OPERATIVO
set tabla_trans=TRANS_CAPTACION_OPERATIVO
set sp=SPM_INT_CAPTACION_OPERATIVO
set formatfile=RMCICAPO.fmt
set errorfile=RMCICAPO_ERROR.txt
set txtfile=RMCICAPO.txt
set batfile=RMCICAPO.bat

set reporteria=\\10.63.30.92\reporteria_alnova\MEXICO\REPORTERIA\CONCILIACION\cifras control\CAPTACION

FOR /F "skip=2 tokens=1" %%d in ('osql -Q "set nocount on; SELECT TOP 1 ID_INSUMO FROM CAT_INSUMOS WHERE NOMBRE = '%batfile%'" -S %server%^,%puerto% -d %bd_tablero% -U %user% -P %pass%'
) DO set idInterfaz=%%d

set bandera=1 

set odate=%1
set yyyy=20%odate:~0,2%
set yy=%odate:~0,2%
set mm=%odate:~2,2%
set dd=%odate:~4,2%

set FechaT=%yyyy%-%mm%-%dd%

set finterfaz=%yyyy%-%mm%-%dd%

set interfaz=CAPTACIONOPERATIVO%finterfaz%.txt
set interfazRAR=CAPTACIONOPERATIVO%finterfaz%.txt.rar

IF "%bandera%" == "1 " GOTO copiainterfaz
ECHO Bandera no activa
EXIT

:copiainterfaz
	IF exist "C:\Program Files\WinRAR\Rar.exe" (copy "C:\Program Files\WinRAR\Rar.exe" %carpetaM%)
	
	IF exist "%reporteria%\%interfaz%" (copy "%reporteria%\%interfaz%" %carpetaM%) ELSE (copy "%reporteria%\%interfazRAR%" %carpetaM%)
	
	%unidadControlM%:
	CD\
	CD ALTAM\MAZP\BATCH\RAZ\
	
	IF exist "%interfazRAR%" (Rar.exe e %interfazRAR%)
	
	set error=FALLO PROCESO, ERROR B99Tx0013
IF not exist "%interfaz%" (goto incidencia) ELSE goto procesainterfaz

:procesainterfaz
		%unidadControlM%:
		CD\
		CD ALTAM\MAZP\BATCH\RAZ\

		set error=FALLO PROCESO, ERROR B00Tx0005
		osql -Q "EXEC dbo.SPSICRE_PORCENTAJE %idInterfaz%,'%FechaT:~0,10%','%usuario%',0" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia
		
		set error=FALLO PROCESO, ERROR B03Tx0008
		osql -Q "EXEC dbo.SPSICRE_PROCESO_CARGAS 3,'%FechaT:~0,10%',%idInterfaz%,'%usuario%',1,1" -S %server%^,%puerto% -d %bd_tablero% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia
		
		set contador=""
		set error=FALLO PROCESO, ERROR B99Tx0003 
		osql -Q "EXEC dbo.PATRUNCATE 'DBO.%tabla_trans%'" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b

		FOR /F "skip=2 tokens=1" %%c in ('osql -Q "SET NOCOUNT ON; SELECT COUNT(1) FROM DBO.%tabla_trans% WITH(NOLOCK)"  -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass%'
			) DO ( set contador=%%c )
		if "%contador%" NEQ "0 " goto incidencia
		
		for %%A in (%interfaz%) do set size=%%~zA
		
		set error=FALLO PROCESO, ERROR B99Tx0014
		for /f %%a in ('type "%interfaz%"^|find "" /v /c') do set /a registros=(%%a-3)
		osql -Q "EXEC dbo.SPSICRE_CIFRAS_CONTROL 5,'%FechaT:~0,10%',%idInterfaz%,%registros%,'%usuario%' " -S %server%^,%puerto% -d %bd_tablero% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia

		set error=FALLO PROCESO, ERROR B99Tx0001
		BCP %bd_sicre%.dbo.%tabla_trans% IN  %interfaz% -f %formatfile% -F 2 -e %errorfile% -S %server%^,%puerto% -U %user% -P %pass% > %txtfile%
		if %errorlevel% NEQ 0 goto incidencia

		set error=FALLO PROCESO, ERROR B02Tx0007
		osql -Q "EXEC dbo.SPSICRE_PROCESO_CARGAS 2,'%FechaT:~0,10%',%idInterfaz%,'%usuario%',1,1" -S %server%^,%puerto% -d %bd_tablero% -U %user% -P %pass% -b 
		if %errorlevel% NEQ 0 goto incidencia
		
		set error=FALLO PROCESO, ERROR B99Tx0011
		osql -Q "EXEC dbo.SPSICRE_VALIDACIONES_E1_ESTRUC %idInterfaz%,'%usuario%','%FechaT:~0,10%'" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia
		
		set error=FALLO PROCESO, ERROR B99Tx0004
		osql -Q "EXEC dbo.%sp% '%FechaT:~0,10%'" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia
		
		set error=FALLO PROCESO, ERROR B99Tx0010
		osql -Q "EXEC dbo.SPSICRE_VALIDACIONES_E1 %idInterfaz%,'%usuario%','%FechaT:~0,10%'" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia
		
		set error=FALLO PROCESO, ERROR B99Tx0015
		for /F "tokens=1 delims= " %%i in ('findstr "copied." %txtfile%') do (
			set registrosBCP=%%i)
		osql -Q "EXEC dbo.SPSICRE_CIFRAS_CONTROL 6,'%FechaT:~0,10%',%idInterfaz%,%registrosBCP%,'%usuario%' " -S %server%^,%puerto% -d %bd_tablero% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia
		
		set error=FALLO PROCESO, ERROR B10Tx0009
		osql -Q "EXEC dbo.SPSICRE_VALIDACION_AGRUPADO 10,'%FechaT:~0,10%',%idInterfaz%,0,0,0,'%usuario%',0" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia
		
		set error=FALLO PROCESO, ERROR B02Tx0006
		osql -Q "EXEC dbo.SPSICRE_PORCENTAJE %idInterfaz%,'%FechaT:~0,10%','%usuario%',2" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia'%FechaT:~0,10%','%usuario%',2" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		echo Borrado de Archivo 
		IF exist "%interfaz%.rar" del %carpetaM%%interfaz%.rar
		IF exist "%interfaz%" del %carpetaM%%interfaz%
		
		EXIT

:incidencia
osql -Q "EXEC dbo.SPSICRE_VALIDACION_AGRUPADO 10,'%FechaT:~0,10%',%idInterfaz%,0,0,0,'%usuario%',0" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass%
osql -Q "EXEC dbo.SPSICRE_PROCESO_CARGAS_ERRORES 0, '%FechaT:~0,10%', %idInterfaz%, '%usuario%',1,'%error%'" -S %server%^,%puerto% -d %bd_tablero% -U %user% -P %pass%
osql -Q "EXEC dbo.SPSICRE_PORCENTAJE %idInterfaz%,'%FechaT:~0,10%','%usuario%',3" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass%
OSQL -Q "INSERT INTO [%bd_sicre%].[dbo].[BIT_ERROR_SP] (PROCEDIMIENTO,DESCRIPCION,USUARIO_ALTA,FECHA_ALTA) VALUES ('CARGA BAT','%error% (%FechaT%)','%usuario%', CONVERT(VARCHAR(23),GETDATE(),121))" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass%
echo El bat finalizo con ERROR
IF exist "%interfaz%.rar" del %carpetaM%%interfaz%.rar
IF exist "%interfaz%" del %carpetaM%%interfaz%

EXIT %errorlevel%

