set unidadControlM=D
%unidadControlM%:
CD\
CD ALTAM\MAZP\BATCH\RAZ\

set txt=RAZ_BD_MEX.config

for /F "tokens=2 delims= " %%i in ('findstr "server=" %txt%') do (
	set server_config=%%i)
for /F "tokens=2 delims= " %%i in ('findstr "puerto=" %txt%') do (
	set puerto_config=%%i)
for /F "tokens=2 delims= " %%i in ('findstr "user=" %txt%') do (
	set user_config=%%i)	
for /F "tokens=2 delims= " %%i in ('findstr "pass=" %txt%') do (
	set pass_config=%%i)

set server=%server_config%
set puerto=%puerto_config%
set user=%user_config%
set pass=%pass_config%
set usuario=Control-M
set carpetaM=%unidadControlM%:\ALTAM\MAZP\BATCH\RAZ\

set bd_tablero=TABLERO_MEXICO
set bd_sicre=SICRE_MEXICO
set tabla_trans1=TRANS_CRED_ALNOVA1
set tabla_trans2=TRANS_CRED_ALNOVA2
set sp=SPM_INT_CRED_ALNOVA
set formatfile1=RMCIALNO1.fmt
set formatfile2=RMCIALNO2.fmt
set script=RMCIALNO.vbs
set errorfile=RMCIALNO_ERROR.txt
set txtfile=RMCIALNO.txt
set batfile=RMCIALNO.bat

set reporteria=\\10.63.30.92\reporteria_alnova\MEXICO\REPORTERIA\Prestamos_Alnova

FOR /F "skip=2 tokens=1" %%d in ('osql -Q "set nocount on; SELECT TOP 1 ID_INSUMO FROM CAT_INSUMOS WHERE NOMBRE = '%batfile%'" -S %server%^,%puerto% -d %bd_tablero% -U %user% -P %pass%'
) DO set idInterfaz=%%d

FOR /F "skip=2 tokens=1" %%a in ('osql -Q "EXEC dbo.SPSICRE_CONSULTA_BANDERA %idInterfaz%,'%usuario%'" -S %server%^,%puerto% -d %bd_tablero% -U %user% -P %pass%'
) DO ( set bandera=%%a )

FOR /F "skip=2 tokens=2" %%b in ('osql -Q "EXEC dbo.SPSICRE_CONSULTA_BANDERA %idInterfaz%,'%usuario%'" -S %server%^,%puerto% -d %bd_tablero% -U %user% -P %pass%'
) DO (  set FechaT=%%b )

set odate=%FechaT%
set yyyy=%odate:~0,4%
set yy=%odate:~2,2%
set mm=%odate:~5,2%
set dd=%odate:~8,2%

set finterfaz=%yy%%mm%%dd%

set interfaz=BAT1SB60.RESUMEN.CARTERADELIM.D%finterfaz%

set interfazrepro=REPRO.RESUMEN.CARTERADELIM.D%finterfaz%

set interfazori=BAT1SB60.RESUMEN.CARTERADELIM.D%finterfaz%

IF  "%bandera%" == "1 " goto copiainterfaz
Echo Bandera no activa
EXIT

:copiainterfaz
	IF exist "C:\Program Files\WinRAR\Rar.exe" (copy "C:\Program Files\WinRAR\Rar.exe" %carpetaM%)
	
	IF exist "%reporteria%\%interfaz%" (copy %reporteria%\%interfaz% %carpetaM%) ELSE (copy %reporteria%\%interfaz%.rar %carpetaM%)
	
	%unidadControlM%:
	CD\
	CD ALTAM\MAZP\BATCH\RAZ\
	
	IF exist "%interfaz%.rar" (Rar.exe e %interfaz%.rar)
	
	IF exist "%interfazrepro%" (
		copy %carpetaM%%interfazrepro% %carpetaM%%interfazrepro%_
		set interfaz=%interfazrepro%
		) ELSE (
		copy %carpetaM%%interfaz% %carpetaM%%interfaz%_
		)
	
	set error=FALLO PROCESO, ERROR B99Tx0013
IF not exist "%interfaz%" (goto incidencia) ELSE goto procesainterfaz
		
:procesainterfaz
		%unidadControlM%:
		CD\
		CD ALTAM\MAZP\BATCH\RAZ\
		
		set error=FALLO PROCESO, ERROR B00Tx0005
		osql -Q "EXEC dbo.SPSICRE_PORCENTAJE %idInterfaz%,'%FechaT:~0,10%','%usuario%',0" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia
		
		set error=FALLO PROCESO, ERROR B03Tx0008
		osql -Q "EXEC dbo.SPSICRE_PROCESO_CARGAS 3,'%FechaT:~0,10%',%idInterfaz%,'%usuario%',1,1" -S %server%^,%puerto% -d %bd_tablero% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia
		
		set contador=""
		set error=FALLO PROCESO, ERROR B99Tx0003FALLO PROCESO, ERROR B99Tx0012
		osql -Q "EXEC dbo.PATRUNCATE 'DBO.%tabla_trans1%'" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia
		
		osql -Q "EXEC dbo.PATRUNCATE 'DBO.%tabla_trans2%'" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia
		
		set error=FALLO PROCESO, ERROR B99Tx0002
		osql -Q "EXEC dbo.PADELETE 'INT_CRED_ALNOVA1','%FechaT%' " -S %server%,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia

		osql -Q "EXEC dbo.PADELETE 'INT_CRED_ALNOVA2','%FechaT%' " -S %server%,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia

		osql -Q "EXEC dbo.PADELETE 'INT_CRED_ALNOVA1_CANCONT','%FechaT%' " -S %server%,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia

		osql -Q "EXEC dbo.PADELETE 'INT_CRED_ALNOVA2_CANCONT','%FechaT%' " -S %server%,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia

		osql -Q "EXEC dbo.PADELETE 'INT_CRED_ALNOVA1_LIQUIDADOS','%FechaT%' " -S %server%,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia

		osql -Q "EXEC dbo.PADELETE 'INT_CRED_ALNOVA2_LIQUIDADOS','%FechaT%' " -S %server%,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia
		
		set error=FALLO PROCESO, ERROR B99Tx0014
		for /f %%a in ('type "%interfaz%"^|find "" /v /c') do set /a registros=(%%a)
				osql -Q "EXEC dbo.SPSICRE_CIFRAS_CONTROL 5,'%FechaT:~0,10%',%idInterfaz%,%registros%,'%usuario%' " -S %server%^,%puerto% -d %bd_tablero% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia

		set error=FALFALLO PROCESO, ERROR B99Tx0012
		cscript %script% %interfaz% %interfaz%_
		if %errorlevel% NEQ 0 goto incidencia
		
		set error=FALLO PROCESO, ERROR B99Tx0001
		BCP %bd_sicre%.dbo.%tabla_trans1% IN  %interfaz% -f %formatfile1% -e %errorfile% -S %server%^,%puerto% -U %user% -P %pass% > %txtfile%
		if %errorlevel% NEQ 0 goto incidencia
		
		BCP %bd_sicre%.dbo.%tabla_trans2% IN  %interfaz% -f %formatfile2% -e %errorfile% -S %server%^,%puerto% -U %user% -P %pass% -m 0
		if %errorlevel% NEQ 0 goto incidencia
		
		set error=FALLO PROCESO, ERROR B02Tx0007
		osql -Q "EXEC dbo.SPSICRE_PROCESO_CARGAS 2,'%FechaT:~0,10%',%idInterfaz%,'%usuario%',1,1" -S %server%^,%puerto% -d %bd_tablero% -U %user% -P %pass% -b 
		if %errorlevel% NEQ 0 goto incidencia
		
		set error=FALLO PROCESO, ERROR B99Tx0011
		osql -Q "EXEC dbo.SPSICRE_VALIDACIONES_E1_ESTRUC %idInterfaz%,'%usuario%','%FechaT:~0,10%'" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia
		
		set error=FALLO PROCESO, ERROR B99Tx0004
		osql -Q "EXEC dbo.%sp% '%FechaT:~0,10%'" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia
		
		set error=FALLO PROCESO, ERROR B99Tx0010
		osql -Q "EXEC dbo.SPSICRE_VALIDACIONES_E1 %idInterfaz%,'%usuario%','%FechaT:~0,10%'" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia

		set error=FALLO PROCESO, ERROR B99Tx0015
		for /F "tokens=1 delims= " %%i in ('findstr "copied." %txtfile%') do (
			set registrosBCP=%%i)
		osql -Q "EXEC dbo.SPSICRE_CIFRAS_CONTROL 6,'%FechaT:~0,10%',%idInterfaz%,%registrosBCP%,'%usuario%' " -S %server%^,%puerto% -d %bd_tablero% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia
		
		set error=FALLO PROCESO, ERROR B10Tx0009
		osql -Q "EXEC dbo.SPSICRE_VALIDACION_AGRUPADO 10,'%FechaT:~0,10%',%idInterfaz%,0,0,0,'%usuario%',0" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia
		
		set error=FALLO PROCESO, ERROR B02Tx0006
		osql -Q "EXEC dbo.SPSICRE_PORCENTAJE %idInterfaz%,'%FechaT:~0,10%','%usuario%',2" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidencia

		echo Borrado de Archivo 
		IF exist "%interfaz%.rar" del %carpetaM%%interfaz%.rar
		IF exist "%interfazori%.rar" del %carpetaM%%interfazori%.rar
		IF exist "%interfazori%" del %carpetaM%%interfazori%
		IF exist "%interfazori%_" del %carpetaM%%interfazori%_
		IF exist "%interfazrepro%" del %carpetaM%%interfazrepro%
		IF exist "%interfazrepro%_" del %carpetaM%%interfazrepro%_
		
		exit
		

:incidencia
osql -Q "EXEC dbo.SPSICRE_VALIDACION_AGRUPADO 10,'%FechaT:~0,10%',%idInterfaz%,0,0,0,'%usuario%',0" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass%
osql -Q "EXEC dbo.SPSICRE_PROCESO_CARGAS_ERRORES 0, '%FechaT:~0,10%', %idInterfaz%, '%usuario%',1,'%error%'" -S %server%^,%puerto% -d %bd_tablero% -U %user% -P %pass%
osql -Q "EXEC dbo.SPSICRE_PORCENTAJE %idInterfaz%,'%FechaT:~0,10%','%usuario%',3" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass%
OSQL -Q "INSERT INTO [%bd_sicre%].[dbo].[BIT_ERROR_SP] (PROCEDIMIENTO,DESCRIPCION,USUARIO_ALTA,FECHA_ALTA) VALUES ('CARGA BAT','%error% (%FechaT%)','%usuario%', CONVERT(VARCHAR(23),GETDATE(),121))" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass%
echo El bat finalizo con ERROR
IF exist "%interfaz%.rar" del %carpetaM%%interfaz%.rar
IF exist "%interfazori%.rar" del %carpetaM%%interfazori%.rar
IF exist "%interfazori%" del %carpetaM%%interfazori%
IF exist "%interfazori%_" del %carpetaM%%interfazori%_
IF exist "%interfazrepro%" del %carpetaM%%interfazrepro%
IF exist "%interfazrepro%_" del %carpetaM%%interfazrepro%_

exit %errorlevel%