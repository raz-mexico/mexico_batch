set unidadControlM=D
%unidadControlM%:
CD\
CD ALTAM\MAZP\BATCH\RAZ\

set txt=RAZ_BD_MEX.config

for /F "tokens=2 delims= " %%i in ('findstr "server=" %txt%') do (
	set server_config=%%i)
for /F "tokens=2 delims= " %%i in ('findstr "puerto=" %txt%') do (
	set puerto_config=%%i)
for /F "tokens=2 delims= " %%i in ('findstr "user=" %txt%') do (
	set user_config=%%i)	
for /F "tokens=2 delims= " %%i in ('findstr "pass=" %txt%') do (
	set pass_config=%%i)

set server=%server_config%
set puerto=%puerto_config%
set user=%user_config%
set pass=%pass_config%
set usuario=Control-M
set carpetaM=%unidadControlM%:\ALTAM\MAZP\BATCH\RAZ\

set bd_tablero=TABLERO_MEXICO
set bd_sicre=SICRE_MEXICO

set batfile=RMCMACAP.bat

FOR /F "skip=2 tokens=1" %%d in ('osql -Q "set nocount on; SELECT TOP 1 ID_INSUMO FROM CAT_INSUMOS WHERE NOMBRE = '%batfile%'" -S %server%^,%puerto% -d %bd_tablero% -U %user% -P %pass%'
) DO set idMetodologia=%%d

set idFase4=%idMetodologia%
set /A idFase5=%idMetodologia%+1
set /A idFase6=%idMetodologia%+2

set bandera=1 

set odate=%1
set yyyy=20%odate:~0,2%
set yy=%odate:~0,2%
set mm=%odate:~2,2%
set dd=%odate:~4,2%

set FechaT=%yyyy%-%mm%-%dd%

set finterfaz=%yy%%mm%%dd%

IF  "%idMetodologia%" NEQ "" goto procesametodologia
Echo Bandera no activa
EXIT

:procesametodologia
		
		REM SE PROCESA LA FASE 4
		set error=FALLO PROCESO, ERROR SPSICRE_PROCESO_CARGAS
		osql -Q "EXEC dbo.SPSICRE_PROCESO_CARGAS 1,'%FechaT:~0,10%',%idFase4%,'%usuario%',0,2" -S %server%^,%puerto% -d %bd_tablero% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidenciaFase4
	
		set error=FALLO PROCESO, ERROR SPSICRE_PORCENTAJE	
		osql -Q "EXEC dbo.SPSICRE_PORCENTAJE %idFase4%,'%FechaT:~0,10%','%usuario%',0" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidenciaFase4
		
		REM Se obtiene el esatus de si falta informacion de un producto o no
		set error=FALTA INFORMACION DE PRODUCTO
		FOR /F "skip=2 tokens=1" %%d in ('osql -Q"set nocount on; SELECT TOP 1 1 FROM CAT_CAPTA_AGRUPADOR WHERE DESCRIPCIONSUBPROD IS NULL" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass%'
		) DO set ProductoEstatus='%%d'
		if '%ProductoEstatus%'=='1' goto incidenciaFase4

		set error=FALLO PROCESO, ERROR SPSICRE_PORCENTAJE	
		osql -Q "EXEC dbo.SPSICRE_PORCENTAJE %idFase4%,'%FechaT:~0,10%','%usuario%',4" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidenciaFase4
		
		REM SE PROCESA LA FASE 5
		set error=FALLO PROCESO, ERROR SPSICRE_PROCESO_CARGAS
		osql -Q "EXEC dbo.SPSICRE_PROCESO_CARGAS 1,'%FechaT:~0,10%',%idFase5%,'%usuario%',0,2" -S %server%^,%puerto% -d %bd_tablero% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidenciaFase5
	
		set error=FALLO PROCESO, ERROR SPSICRE_PORCENTAJE	
		osql -Q "EXEC dbo.SPSICRE_PORCENTAJE %idFase5%,'%FechaT:~0,10%','%usuario%',5" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidenciaFase5
		
		set error=FALLO PROCESO, ERROR SPSICRE_PORCENTAJE	
		osql -Q "EXEC dbo.SPSICRE_PORCENTAJE %idFase5%,'%FechaT:~0,10%','%usuario%',6" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidenciaFase5
		
		REM SE PROCESA LA FASE 6
		set error=FALLO PROCESO, ERROR SPSICRE_PROCESO_CARGAS
		osql -Q "EXEC dbo.SPSICRE_PROCESO_CARGAS 1,'%FechaT:~0,10%',%idFase6%,'%usuario%',0,2" -S %server%^,%puerto% -d %bd_tablero% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidenciaFase6
	
		set error=FALLO PROCESO, ERROR SPSICRE_PORCENTAJE	
		osql -Q "EXEC dbo.SPSICRE_PORCENTAJE %idFase6%,'%FechaT:~0,10%','%usuario%',5" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidenciaFase6
		
		set error=FALLO PROCESO, ERROR SPSICRE_PORCENTAJE	
		osql -Q "EXEC dbo.SPSICRE_PORCENTAJE %idFase6%,'%FechaT:~0,10%','%usuario%',6" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass% -b
		if %errorlevel% NEQ 0 goto incidenciaFase6
						
		exit
		

:incidenciaFase4
osql -Q "EXEC dbo.SPSICRE_VALIDACION_AGRUPADO 10,'%FechaT:~0,10%',%idFase4%,0,0,0,'%usuario%',0" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass%
osql -Q "EXEC dbo.SPSICRE_PORCENTAJE %idFase4%,'%FechaT:~0,10%','%usuario%',3" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass%

exit 

:incidenciaFase5
osql -Q "EXEC dbo.SPSICRE_VALIDACION_AGRUPADO 10,'%FechaT:~0,10%',%idFase5%,0,0,0,'%usuario%',0" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass%
osql -Q "EXEC dbo.SPSICRE_PORCENTAJE %idFase5%,'%FechaT:~0,10%','%usuario%',3" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass%

exit

:incidenciaFase6
osql -Q "EXEC dbo.SPSICRE_VALIDACION_AGRUPADO 10,'%FechaT:~0,10%',%idFase6%,0,0,0,'%usuario%',0" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass%
osql -Q "EXEC dbo.SPSICRE_PORCENTAJE %idFase6%,'%FechaT:~0,10%','%usuario%',3" -S %server%^,%puerto% -d %bd_sicre% -U %user% -P %pass%

exit