set unidadControlM=D
%unidadControlM%:
CD\
CD ALTAM\MAZP\BATCH\RAZ\

set txt=RAZ_BD_MEX.config

for /F "tokens=2 delims= " %%i in ('findstr "server=" %txt%') do (
	set server_config=%%i)
for /F "tokens=2 delims= " %%i in ('findstr "puerto=" %txt%') do (
	set puerto_config=%%i)
for /F "tokens=2 delims= " %%i in ('findstr "user=" %txt%') do (
	set user_config=%%i)	
for /F "tokens=2 delims= " %%i in ('findstr "pass=" %txt%') do (
	set pass_config=%%i)

set server=%server_config%
set puerto=%puerto_config%
set user=%user_config%
set pass=%pass_config%
set usuario=Control-M
set carpetaM=%unidadControlM%:\ALTAM\MAZP\BATCH\RAZ\

set bd_sicre_ct=SICRE_MEXICO_CT
set bd_tablero=TABLERO_MEXICO
set sp=SPM_CAT_CRED_MAESTRA 
set batfile=RMCIGOBM.bat

FOR /F "tokens=1" %%c in ('osql -Q "set nocount on; SELECT CONVERT(VARCHAR(10),EOMONTH(GETDATE(),-1),120)
" -S %server%^,%puerto% -d %bd_tablero% -U %user% -P %pass%'
) DO set Fecha=%%c 

set odate=%Fecha%

set FechaT=%odate%


	set error=FALLO PROCESO, ERROR B99Tx0004
	osql -Q "EXEC dbo.%sp% '%FechaT:~0,10%'" -S %server%^,%puerto% -d %bd_sicre_ct% -U USRPORTALWEB -P Sv24bd$2008Web -b
	if %errorlevel% NEQ 0 goto incidencia
	
	exit